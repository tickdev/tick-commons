package com.tick;

public final class TickConstants {
	public static final String URL_CONFIG = "/configs";
	public static final String URL_JOB = "/jobs";
	public static final String URL_JOBHISTORY = "/jobhistory";
	public static final String URL_TESTCASE = "/testcases";
	public static final String URL_IMAGE = "/images";
	
	public static final String CONFIG_GLOBAL = "GLOBAL";
	
	public static final String URL_NEXT_ID = "/next-id";
	
	public static final String IMAGE_TYPE = "image/png";

}
