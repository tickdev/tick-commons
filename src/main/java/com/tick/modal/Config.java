package com.tick.modal;

import java.util.HashMap;
import java.util.Map;

import org.springframework.data.annotation.Id;

import lombok.Data;

@Data
public class Config {
	@Id
	private String id;

	private Map<String, String> params = new HashMap<String, String>();
}
