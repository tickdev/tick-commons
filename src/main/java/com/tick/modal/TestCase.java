package com.tick.modal;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.data.annotation.Id;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class TestCase {

	@Id
	private String id;
	
	private String name;
	
	private String priority;
	
	private String functionalArea;
	
	private String summary;

	private String startUrl;

	private List<Precondition> preconditions = new ArrayList<Precondition>();

	private Map<String, String> params = new HashMap<String, String>();
	
	private List<Step> steps;
}