package com.tick.modal.constants;

public enum StepResultStatus {
	PASS, FAIL;
}
