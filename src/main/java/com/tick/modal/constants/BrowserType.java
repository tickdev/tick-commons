package com.tick.modal.constants;

public enum BrowserType {
	Firefox, Chrome, IE, PHANTOM;
}
