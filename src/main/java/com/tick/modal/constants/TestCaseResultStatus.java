package com.tick.modal.constants;

public enum TestCaseResultStatus {
	PASS, FAIL, PRECONDITIONS_FAILURE, ERROR;
}
