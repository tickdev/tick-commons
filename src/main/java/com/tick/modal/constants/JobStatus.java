package com.tick.modal.constants;

public enum JobStatus {
	NEW, RUNNING, COMPLETE;
}
