package com.tick.modal.result;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.data.annotation.Id;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class JobHistory {
	@Id
	private String id;
	
	private String jobId;
	
	private Date startTime;
	
	private Date finishTime;
	
	private List<TestCaseExecution> executions = new ArrayList<TestCaseExecution>();
	
}