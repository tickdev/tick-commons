package com.tick.modal.result;

import java.util.List;

import com.tick.modal.constants.TestCaseResultStatus;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class TestCaseExecution {

	private String testCaseId;
	
	private TestCaseResultStatus status;

	private List<TestCaseExecution> preconditionResults;
	
	private List<StepResult> stepResults;
	
	private String message;
	
}
