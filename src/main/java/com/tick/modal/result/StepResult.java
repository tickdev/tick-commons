package com.tick.modal.result;

import com.tick.modal.constants.StepResultStatus;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
@AllArgsConstructor
public class StepResult {
	private String id;
	
	private String value;
	
	private StepResultStatus status;
}
