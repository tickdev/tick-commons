package com.tick.modal.result;

import java.util.Date;
import java.util.List;

import org.springframework.data.annotation.Id;

import com.tick.modal.constants.BrowserType;
import com.tick.modal.constants.JobStatus;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class Job {
	@Id
	private String id;
	
	private BrowserType browser;
	
	private Date executionTime;

	private List<String> testCases;
	
	private JobStatus status;
	
}
