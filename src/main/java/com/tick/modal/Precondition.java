package com.tick.modal;

import java.util.HashMap;
import java.util.Map;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class Precondition {

	private String testCaseId;

	private Map<String, String> params = new HashMap<String, String>();

}