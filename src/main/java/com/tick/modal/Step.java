package com.tick.modal;


import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class Step {
	private String id;

	private String name;

	private String className;

	private String partialLinkText;

	private String cssSelector;

	private String xpath;

	private String tagName;

	private String action;

	private String value;

	private String storeId;
	
	private String assertion;

	private transient Map<String, Object> params = new HashMap<String, Object>();
	
	public boolean isStoreId() {
		return StringUtils.isNotEmpty(storeId);
	}
	
	public boolean isAssertion() {
		return StringUtils.isNotEmpty(assertion);
	}

}